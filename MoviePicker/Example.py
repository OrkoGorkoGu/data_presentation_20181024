import requests
from bs4 import BeautifulSoup

def main():

    # intialize session
    session_requests = requests.session()

    # url we want to access
    url = "https://www.imdb.com/chart/top?ref_=nv_mv_250"

    # request the URL
    response = session_requests.get(url)

    # parse the response object
    page = BeautifulSoup(response.text, "html.parser")

    # get the table from webpage
    tbl = page.find('tbody')

    # loop through each row in the table
    for row in tbl.findChildren('tr'):

        #loop through each object in the row
        for tblData in row.findChildren('td', ):
            
            if tblData.attrs['class']==["titleColumn"]:
                # get title
                mov_title = tblData.find('a').getText()

                # get year (and remove parentheses)           
                mov_yr = tblData.find('span').getText().strip("()")
                
            elif tblData.attrs['class']==["ratingColumn", "imdbRating"]:
                # get IMDB rating
                mov_imdbRating = tblData.find('strong').getText()
       
        print(
            mov_title.strip() + "\t"
            + mov_yr.strip() + "\t"
            + mov_imdbRating.strip()
        )

if __name__=="__main__":
    main()
import requests
from bs4 import BeautifulSoup

def main():
    # open file for output
    outfile = open("Movies.txt", 'w')

    # intialize session
    session_requests = requests.session()

    # url to webpage
    url = "https://www.imdb.com/chart/top?ref_=nv_mv_250"

    # request the URL
    response = session_requests.get(url)

    # parse the response object
    page = BeautifulSoup(response.text, "html.parser")

    # get the table from webpage
    tbl = page.find('tbody')

    # loop through each row in the table
    for row in tbl.findChildren('tr'):
        #loop through each object in the row
        for tblData in row.findChildren('td', ):
            if tblData.attrs['class']==["titleColumn"]:
                # get title and movie link
                mov_title = tblData.find('a').getText()
                mov_link = tblData.find('a')['href']

                # go to the movie webpage
                url = "https://imdb.com" + mov_link
                response = session_requests.get(url)

                # get movie info
                page = BeautifulSoup(response.text, "html.parser")
                subtext = page.find('div', attrs={'class', 'subtext'})
                data = subtext.getText().split("|")
                if len(data) == 4:
                    mov_contentRating, mov_runTime, mov_genre, mov_releaseDate = data
                
                # some of the movies have more than one genre; we'll just take the first
                mov_genre = mov_genre.split(",")[0]

                # get year (and remove parentheses)            
                mov_yr = tblData.find('span').getText().strip("()")
                
            elif tblData.attrs['class']==["ratingColumn", "imdbRating"]:
                # get IMDB rating
                mov_imdbRating = tblData.find('strong').getText()
            
        # write movie to file
        outfile.write(
            mov_title.strip() + "\t"
            + mov_contentRating.strip() + '\t'
            + mov_genre.strip() + "\t"
            + mov_yr.strip() + "\t"
            + mov_imdbRating.strip()
            + "\n"
        )
        
    # close the file
    outfile.close()

if __name__=="__main__":
    main()